import SearchSaga from "../components/search/redux/searchSaga";
import { fork, all } from "redux-saga/effects";

function* RootSaga(){
    yield all([
        fork(SearchSaga)
        // fork(AnotherSaga), for other
    ]);
}

export default RootSaga;