import { combineReducers } from "redux";
import SearchReducer from "../components/search/redux/searchReducer";

export default combineReducers({
    search: SearchReducer
});