import React from "react";
import { connect } from "react-redux";

const Results = ({searchResults}) => {
    if (searchResults) {
        return (
            <>
                {renderSearchResults(searchResults)}
            </>
        )
    }

    return null;
};

const renderSearchResults = searchResults => {
    return searchResults.toJS().map(result => {
        return <div>{console.log(result.toJS())}</div>
    })
}

const mapStateToProps = state => {
    return {
        // state.IME_IZ_ROOT_REDUCER.sta_treba
        searchResults: state.search.get('searchResults')
    }
}

export default connect(mapStateToProps, null)(Results);