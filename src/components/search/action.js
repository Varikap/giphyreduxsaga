import types from "./redux/types";

export const getSearchResults = value => {
    return {
        type: types.watchGetSearchResults,
        value
    }
}

export const setSearchResult = value => {
    return {
        type: types.addSearchResults,
        value
    }
}