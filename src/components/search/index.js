import React from "react";
import { connect } from "react-redux";
import { getSearchResults } from "./action";

const onInputChange = async (e, props) => {
    if (e.target.value.trim() !== '')
        props.getSearchResults(e.target.value);
};

const Search = (props) => {
    console.log(props)
    return (
        <>
            <input onChange={e => onInputChange(e, props)}/>
        </>
    )
};

const mapDispatchToProps = {
    getSearchResults
};

export default connect(null, mapDispatchToProps)(Search);