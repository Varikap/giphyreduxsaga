import { takeLatest, put } from "redux-saga/effects";
import types from "./types";
import axios from "axios";
import { setSearchResult } from "../action";

function* setSearch({value}) {
    // http://api.giphy.com/v1/gifs/search
    try {
        const baseUrl = 'http://api.giphy.com/v1';
        const url = `${baseUrl}/gifs/search?api_key=yGHz5YXqgsGEQKOtz65LNV3jcNQj5hCQ&limit=20&q=${value}`;
        const data = yield axios.get(url);
        // ovako dispatch drugu akciju
        return yield put(setSearchResult(data.data.data));
    } catch(e) {
        console.error(e);
    }
}

function* watchSearch() {
    yield takeLatest(types.watchGetSearchResults, setSearch)
}

export default watchSearch;